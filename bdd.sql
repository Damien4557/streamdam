
USE DamiStream;

CREATE TABLE film
(
    ID               INT                     NOT NULL AUTO_INCREMENT,
    NOM_FILM              VARCHAR(100)       NOT NULL,
    DESCRIPTION_FILM      TEXT,
    IMAGE_FILM            VARCHAR(250),
    CHM_FILM              LONGBLOB,
    GENRE_FILM            VARCHAR(100),
    primary key (ID)
);

CREATE TABLE serie
(
    ID               INT                     NOT NULL AUTO_INCREMENT,
    NOM_SERIE              VARCHAR(100)      NOT NULL,
    NB_SAISON              SMALLINT          NOT NULL,
    DESCRIPTION_SERIE      TEXT,
    IMAGE_SERIE            VARCHAR(250),
    CHM_SERIE              VARCHAR(250),
    GENRE_SERIE            VARCHAR(100),
    primary key (ID)
);


