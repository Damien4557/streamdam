<?php
include "connection.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stream-Dam</title>

</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Oswald&display=swap');
    :root {
        --purple-color: #5F0F40;
        --red-color: #9A031E;
        --yellow-color: #FB8B24;
        --orange-color: #EB7329;
        --blue-color: #0F4C5C;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    body {
        font-family: 'Oswald', sans-serif;
        background: radial-gradient(ellipse farthest-corner at center top, #ffee75, #ff8930);
        
        height: 100vh;
    }

    .container {
        width: 90%;
        display: flex;
        height: 90vh;
        background: #fff;
        border-radius: 25px;
        margin: 5vh auto 0 auto;
        box-shadow: 0 5px 30px rgba(0, 0, 0, .2);
        overflow: hidden;


    }

    .bloc_right {
        width: 100%;
    }

    h1 {
        font-size: 25px;
        text-transform: uppercase;
        text-align: center;
        color: #1c222b;
    }

    .sous_title {
        margin-left: 80px;
        font-size: 35px;
    }

    strong {
        background: var(--orange-color);
        color: #fff;
        display: inline-block;
        border-radius: 30px;
        padding: 6px;
        font-size: 83%;
        line-height: 1;
        position: relative;
        top: -1px;
        left: 2px;
    }

    .navBar {
        padding: 25px 30px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #CECECEAF;
    }

    .icon {
        width: 18px;
        height: 18px;
        cursor: pointer;
    }

    #menu {
        position: relative;
        top: 0;
        bottom: 0;
        left: 0;
        width: 0;
        overflow: hidden;
        background: var(--orange-color);
        z-index: 3;
        transition: all 0.4s;

    }

    .list_menu {
        margin: 25px auto;
        padding: 10px;
        width: auto;


    }

    .list_menu ul {
        list-style: none;
        margin: 25px auto;


    }

    .list_menu ul li {
        padding: 0 10px;
        border-radius: 12px;
        color: #00000078;
        width: 100%;
        font-weight: bolder;
        transition: all 0.3s;
        font-size: 20px;

    }

    .list_menu h2 {
        font-size: 20px;
        font-size: large;
    }

    a {
        text-decoration: none;
        display: flex;
        justify-content: space-between;
        align-items: center;
        align-content: center;
        padding: 10px;
    }

    .icon_menu {

        font-size: 25px;
        background: #00000078;
        padding: 5px;

        border-radius: 10px;
        color: #DFDFDF8E;
        transition: all 0.3s;
    }

    a:hover .icon_menu {
        color: #FFF;
    }

    a:hover:nth-child(1) .icon_menu {
        background: var(--purple-color);
    }

    a:hover:nth-child(2) .icon_menu {
        background: var(--red-color);

    }

    a:hover:nth-child(3) .icon_menu {
        background: var(--blue-color);

    }

    a:hover:nth-child(4) .icon_menu {
        background: var(--yellow-color);
    }

    a:hover li {
        color: #FFF;
    }

    .container_article {
        display: flex;
        justify-content: center;
        width: 80vw;
        margin: 10px auto;
    }

    .container_film {

        flex-wrap: wrap;
        width: 100;
        overflow-y: scroll;

    }

    .container_film:hover {
        overflow: scroll;

    }

    .card_film {
        width: 350px;

        height: 500px;
        margin: 15px;
        border-radius: 12px;
        overflow: hidden;

        transition: all 0.3s;
    }

    .card_film:hover {
        width: 356px;
        height: 506px;
        margin: 12px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
    }
    .card{
        visibility: visible;
        animation: fadeInOut 1s;
        font-size: 25px;
    }
    .card img {
        width: 100%;
        height: 100%;

    }



    .card_film_watched {
        width: 50%;
        height: 25vw;
        margin: 15px;
        border-radius: 12px;
        overflow: hidden;
        transition: all 0.3s;
    }

    .card_film_watched:hover {
        height: 24.5vw;
        width: 49%;
        margin: 20px;
    }

    .card_film_watched:hover img {
        filter: blur(1.5px);
    }

    .information {
        color: #FFF;
        display: flex;
        justify-content: space-between;
        align-items: center;
        transition: all 0.4s;
        padding: 0 15px;
        height: 200px;
        background: linear-gradient(#FFFFFF00, #4A4335A9, #4A4335C8);
    }

    .information_watch {
        color: #FFF;
        display: flex;
        justify-content: space-between;
        align-items: center;
        transition: all 0.4s;
        padding: 0 15px;
        height: 200px;
        opacity: 0;

    }

    .card:hover .information_watch {
        opacity: 1;
    }

    .card:hover .information,
    .information_watch {
        transform: translateY(-80%);

    }

    #container_principal {
        height: 80%;
        overflow: scroll;
        margin-top: 50px
    }

    .card button {
        font-size: 15px;
        border: 0;
        padding: 5px;
        border-radius: 5px;
        background: var(--orange-color);
        cursor: pointer;
    }

    .information_watch button {
        font-size: 20px;
        border: 0;
        padding: 7px;
        border-radius: 10px;
        background: var(--orange-color);
        cursor: pointer;
    }

    #card_select {
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 15px;
        border-radius: 12px;
    }

    #card_select img {
        width: 350px;
        height: 500px;
        border-radius: 12px;
        transition: all 0.3s;
    }

    .information_select {
        background: #FFF;
        border-radius: 12px;
        height: auto;
        width: 45%;
        padding: 20px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
        transform: translateX(-30px);
        transition: all 0.3s;
    }

    #card_select:hover img {
        transform: translateX(20px);
    }

    #card_select:hover .information_select {
        transform: translateX(-50px);
    }

    #icon_retourn {
        font-size: 50px;
        fill: var(--orange-color);
        cursor: pointer;
    }

    .categories {
        width: 80vw;
        margin: 5vh auto;
        display: flex;
        justify-content: center;
        align-content: flex-start;
        flex-wrap: wrap;
    }

    .bloc_categorie {
        width: 250px;
        height: 150px;
        display: grid;
        justify-content: center;
        border-radius: 12px;
        visibility: visible;
        animation: fadeInOut 1s;
        margin: 15px;
        text-align: center;
    }

    .bloc_categorie a {
        text-decoration: none;
        color: #FFF;
    }

    @keyframes fadeInOut {

        0% {
            margin-top: 25px;
            opacity: 0;

        }

        100% {
            margin-top: 15px;
            opacity: 1;
        }


    }
</style>

<body>
    <main class="container">
        <div class="bloc_left" id="menu">
            <div class="list_menu">
                <h2 style="color: #FFF;">Menu</h2>
                <ul>
                    <a onclick="afficheAcueille()" href="">
                        <span>
                            <ion-icon class="icon_menu" name="caret-forward-circle"></ion-icon>
                        </span>
                        <li>Nouveauté</li>
                    </a>
                    <a onclick="openCategories()" href="#">
                        <span>
                            <ion-icon class="icon_menu" name="copy"></ion-icon>
                        </span>
                        <li>Categories</li>
                    </a>
                    <a href="">
                        <span>
                            <ion-icon class="icon_menu" name="albums"></ion-icon>
                        </span>
                        <li>Playlist</li>
                    </a>
                    <a href="">
                        <span>
                            <ion-icon class="icon_menu" name="settings"></ion-icon>
                        </span>
                        <li>Settings</li>
                    </a>
                </ul>
            </div>
            <div class="list_menu">
                <h2 style="color: #FFF;">Categories</h2>
                <ul>
                    <a href="">
                        <span>
                            <ion-icon class="icon_menu" name="caret-forward-circle"></ion-icon>
                        </span>
                        <li>Live</li>
                    </a>
                    <a href="">
                        <span>
                            <ion-icon class="icon_menu" name="copy"></ion-icon>
                        </span>
                        <li>Tutorial</li>
                    </a>
                    <a href="">
                        <span>
                            <ion-icon class="icon_menu" name="albums"></ion-icon>
                        </span>
                        <li>Community</li>
                    </a>
                </ul>
            </div>

        </div>
        </div>
        <div class="bloc_right">
            <nav class="navBar">
                <div><svg version="1.1" class="icon" id="Capa_1" style="transition: all 0.3s;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 384.97 384.97" style="enable-background:new 0 0 384.97 384.97;" xml:space="preserve">
                        <g>
                            <g id="icon_menu">
                                <path d="M12.03,84.212h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03
			C5.39,60.152,0,65.541,0,72.182C0,78.823,5.39,84.212,12.03,84.212z" />
                                <path d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03
			S379.58,180.455,372.939,180.455z" />
                                <path d="M372.939,300.758H12.03c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h360.909
			c6.641,0,12.03-5.39,12.03-12.03C384.97,306.147,379.58,300.758,372.939,300.758z" />
                            </g>

                        </g>

                    </svg></div>
                <div>
                    <h1>Stream<strong>DAM</strong></h1>
                </div>
                <div><svg version="1.1" class="icon" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 487.95 487.95" style="enable-background:new 0 0 487.95 487.95;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M481.8,453l-140-140.1c27.6-33.1,44.2-75.4,44.2-121.6C386,85.9,299.5,0.2,193.1,0.2S0,86,0,191.4s86.5,191.1,192.9,191.1
			c45.2,0,86.8-15.5,119.8-41.4l140.5,140.5c8.2,8.2,20.4,8.2,28.6,0C490,473.4,490,461.2,481.8,453z M41,191.4
			c0-82.8,68.2-150.1,151.9-150.1s151.9,67.3,151.9,150.1s-68.2,150.1-151.9,150.1S41,274.1,41,191.4z" />
                            </g>
                        </g>
                    </svg></div>
            </nav>
            <div id="container_principal">

                <div id="select">
                    <h2 class="sous_title">Nouveauté</h2>
                </div>
                <article id="container_film" class="container_article container_film" style="max-height: 540px;">
                    <div class="card card_film">
                        <img src="https://fr.web.img6.acsta.net/pictures/22/05/09/16/57/0014482.jpg" alt="" srcset="">

                        <div class="information">
                            <h2 class="title">Avatar</h2>
                            <button type="submit" onclick="wishList()">Voir</button>
                        </div>
                    </div>
                    <div class="card card_film">
                        <img src="https://fr.web.img5.acsta.net/pictures/21/05/18/10/40/2487837.jpg" alt="" srcset="">

                        <div class="information">
                            <h2 class="title">Fast And Furious</h2>

                            <button>Voir</button>
                        </div>
                    </div>
                    <div class="card card_film">
                        <img src="https://m.media-amazon.com/images/I/91m8QW0uNVL._RI_.jpg" alt="" srcset="">

                        <div class="information">
                            <h2 class="title">The Gentlemen</h2>

                            <button>Voir</button>
                        </div>
                    </div>
                    <div class="card card_film">

                        <img src="https://www.chroniquedisney.fr/imgAnimation/2010/2019-reine-neiges-II-01-big.jpg" alt="" srcset="">

                        <div class="information">
                            <h2 class="title">La Renne des Neiges</h2>

                            <button>Voir</button>
                        </div>

                    </div>

                </article>
                <h2 class="sous_title">Most Watched</h2>
                <article class="container_article container_watched">
                    <div class="card card_film_watched">
                        <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/dddb8beb-7509-4c66-bc59-5e64fc25d614/d7jlecc-669d2a60-e3ba-4275-8b2b-22ebe9a386d8.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2RkZGI4YmViLTc1MDktNGM2Ni1iYzU5LTVlNjRmYzI1ZDYxNFwvZDdqbGVjYy02NjlkMmE2MC1lM2JhLTQyNzUtOGIyYi0yMmViZTlhMzg2ZDguanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.WR0edpqZflz0P9Xo3fkfF5lpq9GizJ7GZ8Kb1Z3oOEQ" alt="" srcset="">
                        <div class="information_watch">
                            <h2 class="title">Transformers</h2>

                            <button>Voir</button>
                        </div>
                    </div>
                    <div class="card card_film_watched">
                        <img src="https://images.bfmtv.com/eQPv-YNr8rP-maa27VJotB9P51c=/0x0:800x450/800x0/images/PatPatrouille-379193.jpg">
                        <div class="information_watch">
                            <h2 class="title">Pat Patrouille</h2>
                            <button>Voir</button>
                        </div>

                </article>

            </div>
            <div class="container_second">

            </div>
        </div>

    </main>
</body>

</html>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    var menu = document.getElementById('menu');
    var openMenu = document.getElementById('Capa_1');
    var container = document.getElementById("container_film");
    var container_categorie = document.getElementById("container_principal");
    var colorCategories = document.getElementsByClassName('bloc_categorie');
    var tabColors = ['#5F0F40', '#9A031E', '#FB8B24', '#EB7329', '#0F4C5C'];
    var color = ""
    var sousTilte = document.getElementById('select');
    var container = document.getElementById("container_film");
    openMenu.addEventListener('click', function() {
        if (menu.style.width != '250px') {
            menu.style.width = '250px';
            openMenu.style.transform ='rotate(-90deg)';
            openMenu.style.fill = '#EB7329';

        } else {
            menu.style.width = '0vw';
            openMenu.style.transform ='rotate(-180deg)';
            openMenu.style.fill = '#1B1B1B';
        }
    })

    function wishList() {
        sousTilte.innerHTML = '<ion-icon style="margin-left:50px;" id="icon_retourn" name="arrow-back-circle"></ion-icon>';
        container.innerHTML = '<div id="card_select"><img src="https://fr.web.img6.acsta.net/pictures/22/05/09/16/57/0014482.jpg" alt="" srcset=""><div class="information_select"><h2>Avatar</h2><p>Sur le monde extraterrestre luxuriant de Pandora vivent les Navi, des êtres qui semblent primitifs mais qui sont très évolués. Parce que l\'environnement de la planète est toxique, les hybrides humains / Navi, appelés Avatars, doivent se connecter aux esprits humains pour permettre la libre circulation sur Pandora. Jake Sully (Sam Worthington), un ancien Marine paralysé, redevient mobile grâce à l\'un de ces Avatars et tombe amoureux d\'une femme Navi (Zoe Saldana). Alors qu\'un lien avec elle grandit, il est entraîné dans une bataille pour la survie de son monde.</p></div></div>';
    }

    sousTilte.addEventListener('click', function afficheAcueille() {
        container_categorie.classList.remove("categories");
        sousTilte.innerHTML = '<h2 id="select" class="sous_title">Nouveauté</h2>';
        container.innerHTML = '<div class="card card_film"><img src="https://fr.web.img6.acsta.net/pictures/22/05/09/16/57/0014482.jpg" alt="" srcset=""><div class="information"><h2 class="title">Avatar</h2><button type="submit" onclick="wishList()">Voir</button></div></div><div class="card card_film"><img src="https://fr.web.img5.acsta.net/pictures/21/05/18/10/40/2487837.jpg" alt="" srcset=""><div class="information"><h2 class="title">Fast And Furious</h2><button>Voir</button></div></div><div class="card card_film"> <img src="https://m.media-amazon.com/images/I/91m8QW0uNVL._RI_.jpg" alt="" srcset=""><div class="information"><h2 class="title">The Gentlemen</h2><button>Voir</button></div></div><div class="card card_film"><img src="https://www.chroniquedisney.fr/imgAnimation/2010/2019-reine-neiges-II-01-big.jpg" alt="" srcset=""><div class="information"><h2 class="title">La Renne des Neiges</h2><button>Voir</button></div></div>';

    })

    function openCategories() {
        sousTilte.innerHTML = '<ion-icon style="margin-left:50px;" id="icon_retourn" name="arrow-back-circle"></ion-icon>';
        container_categorie.classList.add("categories");
        container_categorie.innerHTML = '<div class="bloc_categorie"><a href=""> Drame</a></div><div class="bloc_categorie"><a href=""> Action</a></div><div class="bloc_categorie"><a href=""> Film d\'horreur</a></div><div class="bloc_categorie"><a href="">Aventure</a> </div><div class="bloc_categorie"><a href="">Science-fiction</a> </div><div class="bloc_categorie"><a href="">Thriller</a> </div><div class="bloc_categorie"><a href=""> Fiction</a></div><div class="bloc_categorie"><a href="">Romance</a> </div><div class="bloc_categorie"><a href=""> Western</a></div><div class="bloc_categorie"><a href="">Comédie</a> </div>';
        for (let j = 0; j <= tabColors.length; j++) {
            for (let index = 0; index <= colorCategories.length; index++) {
                const element = colorCategories[index];
                var nb = Math.floor(Math.random() * tabColors.length);
                element.style.background = tabColors[nb];
            }

        }

    }
</script>